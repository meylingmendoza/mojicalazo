package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Cargo;
import data.TableCargo;

/**
 * Servlet implementation class ServletCargo
 */
@WebServlet("/ServletCargo")
public class ServletCargo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCargo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try
		{
			String opc = request.getParameter("opc");
			if(opc.equals("1")){
				Cargo c = new Cargo();
				TableCargo tc = new TableCargo();
				boolean flag = false;
				System.out.println(flag);
				c.setNombre(request.getParameter("nombre"));
				c.setDescripcion(request.getParameter("descripcion"));
				flag = tc.guardarCargo(c);
				if(flag == true)
				{
					response.sendRedirect("./catalogo/Cargo.jsp");
				}
				else
				{
					response.sendRedirect("./catalogo/Cargo.jsp");
				}
			}
			else if(opc.equals("2")){
				Cargo c = new Cargo();
				TableCargo tc = new TableCargo();
				boolean flag = false;
				
				c.setIdCargo(Integer.parseInt(request.getParameter("idCargo")));
				c.setNombre(request.getParameter("nombreM"));
				c.setDescripcion(request.getParameter("descripcionM"));
				
				flag = tc.modificarCargo(c);
				if(flag == true){
					response.sendRedirect("./catalogo/Cargo.jsp?opc=90");
				}else{
					response.sendRedirect("./catalogo/Cargo.jsp?opc=91");
				}
			}
			else if(opc.equals("3")){
				int id = Integer.parseInt(request.getParameter("idCargoE"));
				TableCargo tc = new TableCargo();
				boolean flag = false;
				
				flag = tc.eliminarCargo(id);
				if(flag == true){
					response.sendRedirect("./catalogo/Cargo.jsp?opc=90");
				}else{
					response.sendRedirect("./catalogo/Cargo.jsp?opc=91");
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
	}

}

package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Especialidad;
import data.TableEspecialidad;

/**
 * Servlet implementation class ServletEspecialidad
 */
@WebServlet("/ServletEspecialidad")
public class ServletEspecialidad extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEspecialidad() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try
		{
			String opc = request.getParameter("opc");
			if(opc.equals("1")){
				Especialidad e = new Especialidad();
				TableEspecialidad tc = new TableEspecialidad();
				boolean flag = false;
				System.out.println(flag);
				e.setNombre(request.getParameter("nombre"));
				e.setDescripcion(request.getParameter("descripcion"));
				flag = tc.guardarEspecialidad(e);
				if(flag == true)
				{
					response.sendRedirect("./catalogo/Especialidad.jsp");
				}
				else
				{
					response.sendRedirect("./catalogo/Especialidad.jsp");
				}
			}
			else if(opc.equals("2")){
				Especialidad e = new Especialidad();
				TableEspecialidad tc = new TableEspecialidad();
				boolean flag = false;
				
				e.setIdEspecialidad(Integer.parseInt(request.getParameter("idEspecialidad")));
				e.setNombre(request.getParameter("nombreM"));
				e.setDescripcion(request.getParameter("descripcionM"));
				
				flag = tc.modificarEspecialidad(e);
				if(flag == true){
					response.sendRedirect("./catalogo/Especialidad.jsp?opc=90");
				}else{
					response.sendRedirect("./catalogo/Especialidad.jsp?opc=91");
				}
			}
			else if(opc.equals("3")){
				int id = Integer.parseInt(request.getParameter("idEspecialidadE"));
				TableEspecialidad tc = new TableEspecialidad();
				boolean flag = false;
				
				flag = tc.eliminarEspecialidad(id);
				if(flag == true){
					response.sendRedirect("./catalogo/Especialidad.jsp?opc=90");
				}else{
					response.sendRedirect("./catalogo/Especialidad.jsp?opc=91");
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

}

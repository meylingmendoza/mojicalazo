package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Area;
import data.TableArea;

/**
 * Servlet implementation class ServletArea
 */
@WebServlet("/ServletArea")
public class ServletArea extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletArea() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try
		{
			String opc = request.getParameter("opc");
			if(opc.equals("1")){
				Area a = new Area();
				TableArea tc = new TableArea();
				boolean flag = false;
				System.out.println(flag);
				a.setNombre(request.getParameter("nombre"));
				a.setDescripcion(request.getParameter("descripcion"));
				flag = tc.guardarArea(a);
				if(flag == true)
				{
					response.sendRedirect("./catalogo/Area.jsp");
				}
				else
				{
					response.sendRedirect("./catalogo/Area.jsp");
				}
			}
			else if(opc.equals("2")){
				Area a = new Area();
				TableArea tc = new TableArea();
				boolean flag = false;
				
				a.setIdArea(Integer.parseInt(request.getParameter("idArea")));
				a.setNombre(request.getParameter("nombreM"));
				a.setDescripcion(request.getParameter("descripcionM"));
				
				flag = tc.modificarArea(a);
				if(flag == true){
					response.sendRedirect("./catalogo/Area.jsp?opc=90");
				}else{
					response.sendRedirect("./catalogo/Area.jsp?opc=91");
				}
			}
			else if(opc.equals("3")){
				int id = Integer.parseInt(request.getParameter("idAreaE"));
				TableArea tc = new TableArea();
				boolean flag = false;
				
				flag = tc.eliminarArea(id);
				if(flag == true){
					response.sendRedirect("./catalogo/Area.jsp?opc=90");
				}else{
					response.sendRedirect("./catalogo/Area.jsp?opc=91");
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
	}

}

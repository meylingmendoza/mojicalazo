package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Cliente;
import data.TableCliente;


/**
 * Servlet implementation class ServletCliente
 */
@WebServlet("/ServletCliente")
public class ServletCliente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCliente() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				//doGet(request, response);
				try
				{
					String opc = request.getParameter("opc");
					if(opc.equals("1")){
						Cliente c = new Cliente();
						TableCliente tc = new TableCliente();
						boolean flag = false;
						
						c.setIdTipoCliente(Integer.parseInt(request.getParameter("tipocliente")));
						c.setNombre(request.getParameter("nombre"));
						c.setApellido(request.getParameter("apellido"));
						c.setDireccion(request.getParameter("direccion"));
						c.setTelefono(Integer.parseInt(request.getParameter("telefono")));
						c.setCedula(request.getParameter("cedula"));
						flag = tc.guardarCliente(c);
						if(flag == true)
						{
							response.sendRedirect("./catalogo/Cliente.jsp?opc=99");
						}
						else
						{
							response.sendRedirect("./catalogo/Cliente.jsp");
						}
					}
					else if(opc.equals("2")){
						Cliente c = new Cliente();
						TableCliente tc = new TableCliente();
						boolean flag = false;
						
						c.setIdCliente(Integer.parseInt(request.getParameter("idCliente")));
						c.setIdTipoCliente(Integer.parseInt(request.getParameter("tipoClienteM")));
						c.setNombre(request.getParameter("nombreM"));
						c.setApellido(request.getParameter("apellidoM"));
						c.setDireccion(request.getParameter("direccionM"));
						c.setTelefono(Integer.parseInt(request.getParameter("telefonoM")));
						c.setCedula(request.getParameter("cedulaM"));
						
						flag = tc.modificarCliente(c);
						if(flag == true){
							response.sendRedirect("./catalogo/Cliente.jsp?opc=90");
						}else{
							response.sendRedirect("./catalogo/Cliente.jsp?opc=91");
						}
					}
					else if(opc.equals("3")){
						int id = Integer.parseInt(request.getParameter("idClienteE"));
						TableCliente tc = new TableCliente();
						boolean flag = false;
						
						flag = tc.eliminarCliente(id);
						if(flag == true){
							response.sendRedirect("./catalogo/Cliente.jsp?opc=90");
						}else{
							response.sendRedirect("./catalogo/Cliente.jsp?opc=91");
						}
					}
				}
				catch(Exception ex)
				{
					System.out.println(ex.getMessage());
				}
				
			}
	
}

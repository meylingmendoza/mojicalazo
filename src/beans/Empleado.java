package beans;

public class Empleado {

	private int IdEmpleado;
	private int IdCargo;
	private int IdUsuario;
	private String Nombre;
	private String Apellido;
	private boolean Estado;
	

	
	public Empleado(int idEmpleado, int idCargo, int idUsuario, String nombre, String apellido, boolean estado) {
		super();
		IdEmpleado = idEmpleado;
		IdCargo = idCargo;
		IdUsuario = idUsuario;
		Nombre = nombre;
		Apellido = apellido;
		Estado = estado;
	}
	
	public Empleado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdEmpleado() {
		return IdEmpleado;
	}

	public void setIdEmpleado(int idEmpleado) {
		IdEmpleado = idEmpleado;
	}

	public int getIdCargo() {
		return IdCargo;
	}

	public void setIdCargo(int idCargo) {
		IdCargo = idCargo;
	}

	public int getIdUsuario() {
		return IdUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		IdUsuario = idUsuario;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	public boolean isEstado() {
		return Estado;
	}

	public void setEstado(boolean estado) {
		Estado = estado;
	}

	
	

	
}

package beans;

public class Cargo {

	private int IdCargo;
	private String Nombre;
	private String Descripcion;
	private boolean Estado;
	
	
	
	
	public Cargo(int idCargo, String nombre, String descripcion, boolean estado) {
		super();
		IdCargo = idCargo;
		Nombre = nombre;
		Descripcion = descripcion;
		Estado = estado;
	}
	
	public Cargo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdCargo() {
		return IdCargo;
	}
	public void setIdCargo(int idCargo) {
		IdCargo = idCargo;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	
	
	
}

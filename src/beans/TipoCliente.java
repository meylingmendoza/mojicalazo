package beans;

public class TipoCliente {
	
	private int IdTipoCliente;
	private String Nombre;
	private String Descripcion;
	private boolean Estado;
	
		
	public TipoCliente(int idTipoCliente, String nombre, String descripcion, boolean estado) {
		super();
		IdTipoCliente = idTipoCliente;
		Nombre = nombre;
		Descripcion = descripcion;
		Estado = estado;
	}
	
	
	public TipoCliente() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getIdTipoCliente() {
		return IdTipoCliente;
	}


	public void setIdTipoCliente(int idTipoCliente) {
		IdTipoCliente = idTipoCliente;
	}


	public String getNombre() {
		return Nombre;
	}


	public void setNombre(String nombre) {
		Nombre = nombre;
	}


	public String getDescripcion() {
		return Descripcion;
	}


	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}


	public boolean isEstado() {
		return Estado;
	}


	public void setEstado(boolean estado) {
		Estado = estado;
	}


	
	
	
	

}

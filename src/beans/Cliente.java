package beans;

public class Cliente {

	private int IdCliente;
	private int IdTipoCliente;
	private String NombreTipoCliente;
	private int IdUsuario;
	private String Nombre;
	private String Apellido;
	private String Direccion;
	private int Telefono;
	private String Cedula;
	private boolean Estado;
	
	
	
	
	public Cliente(int idCliente, int idTipoCliente, int idUsuario, String nombre, String apellido, String direccion, int telefono, String cedula, boolean estado) {
		super();
		IdCliente = idCliente;
		IdTipoCliente = idTipoCliente;
		IdUsuario = idUsuario;
		Nombre = nombre;
		Apellido = apellido;
		Direccion = direccion;
		Telefono = telefono;
		Cedula = cedula;
		Estado = estado;
	}
	
	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(int idCliente) {
		IdCliente = idCliente;
	}

	public int getIdTipoCliente() {
		return IdTipoCliente;
	}

	public void setIdTipoCliente(int idTipoCliente) {
		IdTipoCliente = idTipoCliente;
	}
	
	public String getNombreTipoCliente() {
		return NombreTipoCliente;
	}

	public void setNombreTipoCliente(String nombreTipoCliente) {
		NombreTipoCliente = nombreTipoCliente;
	}

	public int getIdUsuario() {
		return IdUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		IdUsuario = idUsuario;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public int getTelefono() {
		return Telefono;
	}

	public void setTelefono(int telefono) {
		Telefono = telefono;
	}

	public String getCedula() {
		return Cedula;
	}

	public void setCedula(String cedula) {
		Cedula = cedula;
	}

	public boolean isEstado() {
		return Estado;
	}

	public void setEstado(boolean estado) {
		Estado = estado;
	}

	
	
	
	
	
	
	
	
}

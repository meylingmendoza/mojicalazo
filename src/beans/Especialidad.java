package beans;

public class Especialidad {
	
	private int IdEspecialidad;
	private String Nombre;
	private String Descripcion;
	private boolean Estado;
	
	public Especialidad(int idEspecialidad, String nombre, String descripcion, boolean estado) {
		super();
		IdEspecialidad = idEspecialidad;
		Nombre = nombre;
		Descripcion = descripcion;
		Estado = estado;
	}
	
	

	public Especialidad() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getIdEspecialidad() {
		return IdEspecialidad;
	}

	public void setIdEspecialidad(int idEspecialidad) {
		IdEspecialidad = idEspecialidad;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public boolean isEstado() {
		return Estado;
	}

	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	
	

}

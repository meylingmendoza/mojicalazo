package beans;

public class Caso {
	
	private int IdCaso;
	private int IdTipoCaso;
	private int IdTipoCliente;
	private int IdCliente;
	private int IdEmpleado;
	
	public Caso(int idCaso, int idTipoCaso, int idTipoCliente, int idCliente, int idEmpleado) {
		super();
		IdCaso = idCaso;
		IdTipoCaso = idTipoCaso;
		IdTipoCliente = idTipoCliente;
		IdCliente =idCliente;
		IdEmpleado = idEmpleado;

	}

	public int getIdCaso() {
		return IdCaso;
	}

	public void setIdCaso(int idCaso) {
		IdCaso = idCaso;
	}

	public int getIdTipoCaso() {
		return IdTipoCaso;
	}

	public void setIdTipoCaso(int idTipoCaso) {
		IdTipoCaso = idTipoCaso;
	}

	public int getIdTipoCliente() {
		return IdTipoCliente;
	}

	public void setIdTipoCliente(int idTipoCliente) {
		IdTipoCliente = idTipoCliente;
	}

	public int getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(int idCliente) {
		IdCliente = idCliente;
	}

	public int getIdEmpleado() {
		return IdEmpleado;
	}

	public void setIdEmpleado(int idEmpleado) {
		IdEmpleado = idEmpleado;
	}
	
	
	

}

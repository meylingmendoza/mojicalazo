package beans;

public class Area {

	private int IdArea;
	private String Nombre;
	private String Descripcion;
	private boolean Estado;
	
	
	
	
	public Area(int idArea, String nombre, String descripcion, boolean estado) {
		super();
		IdArea = idArea;
		Nombre = nombre;
		Descripcion = descripcion;
		Estado = estado;
	}
	
	public Area() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdArea() {
		return IdArea;
	}
	public void setIdArea(int idArea) {
		IdArea = idArea;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	
	
	
}

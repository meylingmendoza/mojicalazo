package beans;

import java.sql.*;

public class Conexion {

	private Connection cn = null;
	private static String URL = "jdbc:sqlserver://localhost:1433;databaseName=BDMojicaLazo;";
	private static String USER = "sa";
	private static String PASS = "12345";
	
	public Conexion(){
		
	}
	
	public Connection getConnection(){
		if(EstaConectado() == false)
			this.CrearConexion();
		return cn;
	}
	
	public boolean EstaConectado(){
		boolean resp = false;
		try
		{
			if((cn == null) || (cn.isClosed())){
				resp = false;
			}
			else{
				resp = true;
			}
		}
		catch(Exception e){
			
		}
		return resp;
	}
	
	public void CrearConexion(){
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			cn = DriverManager.getConnection(URL, USER, PASS);
			System.out.println("Conectado");
		}
		catch(ClassNotFoundException | SQLException ex){
			cn = null;
			System.out.println("Error al establecer la conexion: " + ex.getMessage());
		}
	}
	
	
	public static void main(String args[]){
		new Conexion().getConnection();
	}
	
}

package data;

import beans.*;
import java.util.*;
import java.sql.*;

public class TableArea extends Conexion{
	
	public ArrayList<Area> CargarAreas(){
		ArrayList<Area> lista = new ArrayList<Area>();
		String query = "SELECT * FROM Area WHERE Estado = 'true'";
		
		try
		{
			Connection cn = getConnection();
			PreparedStatement ps = cn.prepareStatement(query);
			
			ResultSet rs = null;
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Area a = new Area();
				a.setIdArea(rs.getInt("IdArea"));
				a.setNombre(rs.getString("Nombre"));
				a.setDescripcion(rs.getString("Descripcion"));
				a.setEstado(rs.getBoolean("Estado"));
				
				lista.add(a);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex.getMessage());
		}
		return lista;
	}
	
	public boolean guardarArea(Area a){
		boolean flag = false;
		int y = 0;
		
		try
		{
		
			String SQL= ("INSERT INTO Area (Nombre, Descripcion, Estado) VALUES (?, ?, ?)");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setString(1, a.getNombre());
			s.setString(2, a.getDescripcion());
			s.setBoolean(3, true);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Error al guardar " + e.getMessage());
		}
		
		return flag;
	}
	
	public boolean modificarArea(Area a){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Area SET Nombre = ?, Descripcion = ? WHERE IdArea = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setString(1, a.getNombre());
			s.setString(2, a.getDescripcion());
			s.setInt(3, a.getIdArea());
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}
	
	public boolean eliminarArea(int id){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Area SET Estado = 'false' WHERE IdArea = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setInt(1, id);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}

}

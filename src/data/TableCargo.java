package data;

import beans.*;
import java.util.*;
import java.sql.*;

public class TableCargo extends Conexion{
	
	public ArrayList<Cargo> CargarCargos(){
		ArrayList<Cargo> lista = new ArrayList<Cargo>();
		String query = "SELECT * FROM Cargo WHERE Estado = 'true'";
		
		try
		{
			Connection cn = getConnection();
			PreparedStatement ps = cn.prepareStatement(query);
			
			ResultSet rs = null;
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Cargo c = new Cargo();
				c.setIdCargo(rs.getInt("IdCargo"));
				c.setNombre(rs.getString("Nombre"));
				c.setDescripcion(rs.getString("Descripcion"));
				c.setEstado(rs.getBoolean("Estado"));
				
				lista.add(c);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex.getMessage());
		}
		return lista;
	}
	
	public boolean guardarCargo(Cargo c){
		boolean flag = false;
		int y = 0;
		
		try
		{
		
			String SQL= ("INSERT INTO Cargo (Nombre, Descripcion, Estado) VALUES (?, ?, ?)");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setString(1, c.getNombre());
			s.setString(2, c.getDescripcion());
			s.setBoolean(3, true);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Error al guardar " + e.getMessage());
		}
		
		return flag;
	}
	
	public boolean modificarCargo(Cargo c){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Cargo SET Nombre = ?, Descripcion = ? WHERE IdCargo = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setString(1, c.getNombre());
			s.setString(2, c.getDescripcion());
			s.setInt(3, c.getIdCargo());
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}
	
	public boolean eliminarCargo(int id){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Cargo SET Estado = 'false' WHERE IdCargo = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setInt(1, id);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}

}

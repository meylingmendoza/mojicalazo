package data;

import beans.*;
import java.util.*;
import java.sql.*;

public class TableEspecialidad extends Conexion{
	
	public ArrayList<Especialidad> CargarEspecialidades(){
		ArrayList<Especialidad> lista = new ArrayList<Especialidad>();
		String query = "SELECT * FROM Especialidad WHERE Estado = 'true'";
		
		try
		{
			Connection cn = getConnection();
			PreparedStatement ps = cn.prepareStatement(query);
			
			ResultSet rs = null;
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Especialidad e = new Especialidad();
				e.setIdEspecialidad(rs.getInt("IdEspecialidad"));
				e.setNombre(rs.getString("Nombre"));
				e.setDescripcion(rs.getString("Descripcion"));
				e.setEstado(rs.getBoolean("Estado"));
				
				lista.add(e);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex.getMessage());
		}
		return lista;
	}
	
	public boolean guardarEspecialidad(Especialidad e){
		boolean flag = false;
		int y = 0;
		
		try
		{
		
			String SQL= ("INSERT INTO Especialidad (Nombre, Descripcion, Estado) VALUES (?, ?, ?)");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setString(1, e.getNombre());
			s.setString(2, e.getDescripcion());
			s.setBoolean(3, true);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println("Error al guardar " + ex.getMessage());
		}
		
		return flag;
	}
	
	public boolean modificarEspecialidad(Especialidad e){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Especialidad SET Nombre = ?, Descripcion = ? WHERE IdEspecialidad = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setString(1, e.getNombre());
			s.setString(2, e.getDescripcion());
			s.setInt(3, e.getIdEspecialidad());
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}
	
	public boolean eliminarEspecialidad(int id){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Especialidad SET Estado = 'false' WHERE IdEspecialidad = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setInt(1, id);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}

}

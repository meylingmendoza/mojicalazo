package data;

import beans.*;
import java.util.*;
import java.sql.*;

public class TableCliente extends Conexion{
	
	public ArrayList<Cliente> CargarClientes(){
		ArrayList<Cliente> lista = new ArrayList<>();
		String query = "SELECT tc.nombre AS TipoCliente, c.*  FROM Cliente c INNER JOIN TipoCliente tc ON c.IdTipoCliente = tc.IdTipoCliente WHERE c.Estado = 'true'";
		
		try
		{
			Connection cn = getConnection();
			PreparedStatement ps = cn.prepareStatement(query);
			
			ResultSet rs = null;
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Cliente c = new Cliente();
				c.setIdCliente(rs.getInt("IdCliente"));
				c.setIdTipoCliente(rs.getInt("IdTipoCliente"));
				c.setNombreTipoCliente(rs.getString("TipoCliente"));
				c.setIdUsuario(rs.getInt("IdUsuario"));
				c.setNombre(rs.getString("Nombre"));
				c.setApellido(rs.getString("Apellido"));
				c.setDireccion(rs.getString("Direccion"));
				c.setTelefono(rs.getInt("Telefono"));
				c.setCedula(rs.getString("Cedula"));
				c.setEstado(rs.getBoolean("Estado"));
				
				lista.add(c);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex.getMessage());
		}
		return lista;
	}
	
	public ArrayList<TipoCliente> CargarTipoCliente(){
		ArrayList<TipoCliente> lista = new ArrayList<>();
		String query = "SELECT * FROM TipoCliente WHERE Estado = 'true'";
		
		try
		{
			Connection cn = getConnection();
			PreparedStatement ps = cn.prepareStatement(query);
			
			ResultSet rs = null;
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				TipoCliente c = new TipoCliente();
				c.setIdTipoCliente(rs.getInt("IdTipoCliente"));
				c.setNombre(rs.getString("Nombre"));
				c.setDescripcion(rs.getString("Descripcion"));
				c.setEstado(rs.getBoolean("Estado"));
				
				lista.add(c);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex.getMessage());
		}
		return lista;
	}
	
	public boolean guardarCliente(Cliente c){
		boolean flag = false;
		int y = 0;
		
		try
		{
		
			String SQL= ("INSERT INTO Cliente (IdTipoCliente, Nombre, Apellido, Direccion, Telefono, Cedula, Estado) VALUES (?, ?, ?, ?, ?, ?, ?)");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setInt(1, c.getIdTipoCliente());
			s.setString(2, c.getNombre());
			s.setString(3, c.getApellido());
			s.setString(4, c.getDireccion());
			s.setInt(5, c.getTelefono());
			s.setString(6, c.getCedula());
			s.setBoolean(7, true);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Error al guardar " + e.getMessage());
		}
		
		return flag;
	}
	
	
	public boolean modificarCliente(Cliente c){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Cliente SET IdTipoCliente = ?, Nombre = ?, Apellido = ?, Direccion = ?, Telefono = ?, Cedula = ? WHERE IdCliente = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setInt(1, c.getIdTipoCliente());
			s.setString(2, c.getNombre());
			s.setString(3, c.getApellido());
			s.setString(4, c.getDireccion());
			s.setInt(5, c.getTelefono());
			s.setString(6, c.getCedula());
			s.setInt(7, c.getIdCliente());
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}
	
	public boolean eliminarCliente(int id){
		boolean flag = false;
		int y = 0;
		try{
			String SQL= ("UPDATE Cliente SET Estado = 'false' WHERE IdCliente = ?");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setInt(1, id);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return flag;
	}
	

}

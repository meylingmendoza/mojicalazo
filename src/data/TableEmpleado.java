package data;

import beans.*;
import java.util.*;
import java.sql.*;

public class TableEmpleado extends Conexion{
	
	public ArrayList<Empleado> CargarEmpleados(){
		ArrayList<Empleado> lista = new ArrayList<>();
		String query = "SELECT * FROM Empleado WHERE Estado = 'true'";
		
		try
		{
			Connection cn = getConnection();
			PreparedStatement ps = cn.prepareStatement(query);
			
			ResultSet rs = null;
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Empleado e = new Empleado();
				e.setIdEmpleado(rs.getInt("IdEmpleado"));
				e.setIdCargo(rs.getInt("IdCargo"));
				e.setIdUsuario(rs.getInt("IdUsuario"));
				e.setNombre(rs.getString("Nombre"));
				e.setApellido(rs.getString("Apellido"));
				e.setEstado(rs.getBoolean("Estado"));
				
				lista.add(e);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex.getMessage());
		}
		return lista;
	}
	
	public boolean guardarEmpleado(Empleado e){
		boolean flag = false;
		int y = 0;
		
		try
		{
		
			String SQL= ("INSERT INTO Empleado (Nombre, Apellido, Estado) VALUES (?, ?, ?)");
			Connection cn = getConnection();
			PreparedStatement s = cn.prepareStatement(SQL);
		   
			s.setString(1, e.getNombre());
			s.setString(2, e.getApellido());
			s.setBoolean(3, true);
				
			y = s.executeUpdate();
			flag = y > 0;
			
			//Cerramos la conexion
			s.close();
			cn.close();
		}
		
		catch(Exception x)
		{
			x.printStackTrace();
			System.out.println("Error al guardar " + x.getMessage());
		}
		
		return flag;
	}

}

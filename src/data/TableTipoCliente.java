package data;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import beans.*;

public class TableTipoCliente extends Conexion {
	
	public ArrayList<TipoCliente> CargarTipoCliente(){
		ArrayList<TipoCliente> lista = new ArrayList<TipoCliente>();
		String query = "SELECT * FROM TipoCliente WHERE Estado = 'true'";
		
		try
		{
			Connection cn = getConnection();
			PreparedStatement ps = cn.prepareStatement(query);
			
			ResultSet rs = null;
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				TipoCliente c = new TipoCliente();
				c.setIdTipoCliente(rs.getInt("IdTipoCliente"));
				c.setNombre(rs.getString("Nombre"));
				c.setDescripcion(rs.getString("Descripcion"));
				c.setEstado(rs.getBoolean("Estado"));
				
				lista.add(c);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex.getMessage());
		}
		return lista;
	}

}
